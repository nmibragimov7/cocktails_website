const mongoose = require('mongoose');
const config = require('./app/config');
const Cocktail = require('./app/models/Cocktail');
const User = require('./app/models/User');
const {nanoid} = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('cocktails');
    } catch (e) {
        console.log('Collection not found. Drop collections skiped...');
    }

    const [user1, user2, user3] = await User.create({
        username: "admin",
        displayName: "Admin",
        password: "admin",
        mail: "admin@mail.com",
        role: "admin",
        image: "image.png",
        token: nanoid(),
    }, {
        username: "user",
        displayName: "User",
        password: "user",
        mail: "user@mail.com",
        role: "user",
        image: "image.png",
        token: nanoid(),
    }, {
        username: "user2",
        displayName: "User2",
        password: "user",
        mail: "user2@mail.com",
        role: "user",
        image: "image.png",
        token: nanoid(),
    });

    await Cocktail.create({
        user: user2._id,
        name: "Cocktail1",
        image: "image.png",
        publish: false,
        recipe: "Recipe",
        ingredients: [{
            name: "ingredients1",
            amount: "50ml"
        },
        {
            name: "ingredients2",
            amount: "10ml"
        }],
        ratings: [{
            user: user3._id,
            rating: 5
        }]
    }, {
        user: user3._id,
        name: "Cocktail2",
        image: "image.png",
        publish: true,
        recipe: "Recipe",
        ingredients: [{
            name: "ingredients1",
            amount: "50ml"
        },
        {
            name: "ingredients2",
            amount: "100ml"
        }],
        ratings: [{
            user: user2._id,
            rating: 5
        }]
    });

    await db.close();
});