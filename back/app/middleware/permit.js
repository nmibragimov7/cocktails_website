const permit = (...roles) => {
    return (req, res, next) => {
        if(!req.user) {
            res.status(401).send({error: 'Unauthenticated'});
        }
        if(!roles.includes(req.user.role)) {
            res.status(403).send({error: 'Unauthorized'});
        }
   
        next();
    }
};
   
module.exports = permit;