const Cocktail = require('../models/Cocktail');

const permitFound = async (req, res, next) => {
    try {
        cocktail = await Cocktail.findOne({_id: req.params.id});
    } catch (error) {
        return res.status(403).send({error: "Cocktail is not found"});
    }

    req.cocktail = cocktail;

    next();
};

module.exports = permitFound;