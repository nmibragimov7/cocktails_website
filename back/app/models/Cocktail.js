const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    publish: {
        type: Boolean,
        required: true,
        default: false
    },
    recipe: {
        type: String,
        required: true
    },
    ingredients: [{
        name: {
            type: String,
            required: true
        },
        amount: {
            type: String,
            required: true
        }
    }],
    ratings: [{
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        rating: {
            type: Number,
            enum : [0, 1, 2,3 , 4, 5],
            default: 0 
        }
    }]
    }, {versionKey: false});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;