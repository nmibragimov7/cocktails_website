const express = require('express');
const multer = require('multer');
const router = express.Router();
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('./config');
const Cocktail = require('./models/Cocktail');
const auth = require('./middleware/auth');
const permit = require('./middleware/permit');
const mongoose = require('mongoose');
const permitFound = require('./middleware/permitFound');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const createRouter = () =>{
    router.get('/', async (req, res) => {
        try {
            const cocktails = await Cocktail.find();
            res.send(cocktails);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/my_cocktails', auth, async (req, res) => {
        try {
            const cocktails = await Cocktail.find({user: req.user});
            res.send(cocktails);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/:id', async (req, res) => {
        try {
            const cocktail = await Cocktail.findOne({_id: req.params.id});
            let cocktailCopy = {...cocktail._doc};
            
            cocktailCopy.votes = cocktail.ratings.length;
            let sumRatings = 0;
            cocktail.ratings.forEach(rating => {
                sumRatings = sumRatings + rating.rating;
            });
            
            cocktailCopy.averageRating = Math.round(parseFloat(sumRatings/cocktailCopy.votes) * 100) / 100;

            res.send(cocktailCopy);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/', auth, upload.single('image'), async (req, res) => {
        const cocktail = new Cocktail({
            name: req.body.name,
            recipe: req.body.recipe,
            ingredients: JSON.parse(req.body.ingredients).map(ingredient => {
                return JSON.parse(ingredient);
            })
        });

        if(req.file){
            cocktail.image = req.file.filename;
        }
        cocktail.user = req.user._id;

        try{
            await cocktail.save();
            res.send({message: "Your cocktail is being moderated"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', [auth, permit('admin'), permitFound], async (req, res) => {
        await Cocktail.findOneAndRemove({_id: req.params.id}, function (e, docs) { 
            if (e){ 
                return res.status(403).send({error: "Coctail cannot be deleted"});
            } 
        });

        try{
            res.send({message: "Cocktail is deleted successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/:id/publish', [auth, permit('admin'), permitFound], async (req, res) => {
        req.cocktail.publish = true;

        try{
            await req.cocktail.save();
            res.send({message: "Cocktail is published successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });
    
    router.post('/:id/rating', [auth, permitFound], async (req, res) => {
        let newRating = null;
        let isFoundRating = false;
        req.cocktail.ratings.forEach(rating => {
            if(rating.user.toString() === req.user._id.toString()) {
                rating.rating = req.body.rating;
                isFoundRating = true;
            }
        });

        if(!isFoundRating) {
            newRating = {
                user: req.user._id,
                rating: req.body.rating
            }
            req.cocktail.ratings.push(newRating);
        }

        try{
            await req.cocktail.save();
            res.send({message: "Your rate is added successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    return router;
};

module.exports = createRouter;