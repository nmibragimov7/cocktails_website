const express = require("express");
const User = require("./models/User");
const router = express.Router();
const auth = require('./middleware/auth');
const {nanoid} = require('nanoid');
const config = require('./config');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({storage});

const createRouter = () => {
    router.post("/", upload.single('image'), async (req, res) => {
        try {
            const user = new User({
                username: req.body.username,
                displayName: req.body.displayName,
                mail: req.body.mail,
                password: req.body.password,
            });

            if(req.file){
                user.image = req.file.filename;
            }

            user.generateToken();
            await user.save();
            res.send(user);
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.post("/sessions", async (req, res) => {
        const user = await User.findOne({username: req.body.username});
        if (!user) {
            return res.status(400).send({error: "Username not found"});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({error: "Password is wrong"});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    });

    router.delete('/sessions', auth, async (req, res) => {
        const user = req.user;
        const message = {message: 'Success'};

        user.token = nanoid();
        await user.save({validateBeforeSave: false});
        res.send(message);
    });

    return router;
};

module.exports = createRouter;