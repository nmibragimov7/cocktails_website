import axios from "../../axiosApi";
import {loadingOffHandler} from "./loadingActions";
import { push } from "connected-react-router";

export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_USER_COCKTAILS_SUCCESS = 'FETCH_USER_COCKTAILS_SUCCESS';
export const FETCH_USER_COCKTAILS_FAILURE = 'FETCH_USER_COCKTAILS_FAILURE';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const ADD_COCKTAIL_SUCCESS = 'ADD_COCKTAIL_SUCCESS';
export const ADD_COCKTAIL_FAILURE = 'ADD_COCKTAIL_FAILURE';
export const CHANGE_STATUS_SUCCESS = 'CHANGE_STATUS_SUCCESS';
export const CHANGE_STATUS_FAILURE = 'CHANGE_STATUS_FAILURE';
export const REMOVE_COCKTAIL_SUCCESS = 'REMOVE_COCKTAIL_SUCCESS';
export const REMOVE_COCKTAIL_FAILURE = 'REMOVE_COCKTAIL_FAILURE';
export const SET_RATING_SUCCESS = 'SET_RATING_SUCCESS';
export const SET_RATING_FAILURE = 'SET_RATING_FAILURE';

export const resetError = () => {
    return {type: RESET_ERROR}
};
export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};
export const fetchCocktailsSuccess = cocktails => {
    return {type: FETCH_COCKTAILS_SUCCESS, cocktails}
};
export const fetchUserCocktailsSuccess = cocktails => {
    return {type: FETCH_USER_COCKTAILS_SUCCESS, cocktails}
};
export const fetchUserCocktailsFailure = errors => {
    return {type: FETCH_USER_COCKTAILS_FAILURE, errors}
};
export const fetchCocktailSuccess = cocktail => {
    return {type: FETCH_COCKTAIL_SUCCESS, cocktail}
};
export const addCocktailSuccess = (message) => {
    return {type: ADD_COCKTAIL_SUCCESS, message}
};
export const addCocktailFailure = errors => {
    return {type: ADD_COCKTAIL_FAILURE, errors}
};
export const changeStatusSuccess = (message) => {
    return {type: CHANGE_STATUS_SUCCESS, message}
};
export const changeStatusFailure = errors => {
    return {type: CHANGE_STATUS_FAILURE, errors}
};
export const removeCocktailSuccess = (message) => {
    return {type: REMOVE_COCKTAIL_SUCCESS, message}
};
export const removeCocktailFailure = errors => {
    return {type: REMOVE_COCKTAIL_FAILURE, errors}
};
export const setRatingSuccess = (message) => {
    return {type: SET_RATING_SUCCESS, message}
};
export const setRatingFailure = errors => {
    return {type: SET_RATING_FAILURE, errors}
};

export const fetchCocktails = () => {
    return async dispatch => {
        try {
            const response = await axios.get('/cocktails');
            dispatch(fetchCocktailsSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchUserCocktails = () => {
    return async (dispatch, getState) => {
        try {
            let token = "";
            if(getState().users.user) {
                token = getState().users.user.token;
            }
            const headers = {'Authorization': token};
            const response = await axios.get('/cocktails/my_cocktails', {headers});
            dispatch(fetchUserCocktailsSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            dispatch(fetchUserCocktailsFailure(e.response.data));
            console.error(e);
        }
    }
};

export const fetchCocktail = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get(`/cocktails/${id}`);
            dispatch(fetchCocktailSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const addCocktail = (cocktail) => {
    return async (dispatch, getState) => {
        try {
            let token = "";
            if(getState().users.user) {
                token = getState().users.user.token;
            }
            const headers = {'Authorization': token};
            const response = await axios.post(`/cocktails`, cocktail, {headers});
            dispatch(addCocktailSuccess(response.data));
            dispatch(push('/'));
        } catch(e) {
            dispatch(addCocktailFailure(e.response.data));
            console.error(e);
        }
    }
};

export const removeCocktail = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axios.delete(`/cocktails/${id}`, {headers});
            dispatch(removeCocktailSuccess(response.data));
            dispatch(fetchCocktails());
        } catch(e) {
            dispatch(removeCocktailFailure(e.response.data));
            console.error(e);
        }
    }
};

export const changeStatus = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axios.post(`/cocktails/${id}/publish`, null, {headers});
            dispatch(changeStatusSuccess(response.data));
            dispatch(fetchCocktails());
        } catch(e) {
            dispatch(changeStatusFailure(e.response.data));
            console.error(e);
        }
    }
};

export const setRating = (id, rating) => {
    return async (dispatch, getState) => {
        try {
            const newRating = {rating: rating}
            let token = "";
            if(getState().users.user) {
                token = getState().users.user.token;
            }
            const headers = {'Authorization': token};
            const response = await axios.post(`/cocktails/${id}/rating`, newRating, {headers});
            dispatch(setRatingSuccess(response.data));
            dispatch(fetchCocktail(id));
            dispatch(loadingOffHandler());
        } catch(e) {
            dispatch(setRatingFailure(e.response.data));
            console.error(e);
        }
    }
};