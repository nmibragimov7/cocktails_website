import {
    RESET_ERROR,
    RESET_MESSAGE,
    FETCH_COCKTAILS_SUCCESS,
    FETCH_USER_COCKTAILS_SUCCESS,
    FETCH_USER_COCKTAILS_FAILURE,
    FETCH_COCKTAIL_SUCCESS,
    ADD_COCKTAIL_SUCCESS,
    ADD_COCKTAIL_FAILURE,
    CHANGE_STATUS_SUCCESS,
    CHANGE_STATUS_FAILURE,
    REMOVE_COCKTAIL_SUCCESS,
    REMOVE_COCKTAIL_FAILURE,
    SET_RATING_SUCCESS,
    SET_RATING_FAILURE
} from "../actions/cocktailsActions";

const initialState = {
    cocktails: [],
    cocktail: {},
    errors: null,
    message: null
};

const cocktailsReducer = (state = initialState, action) => {
    switch(action.type){
        case RESET_ERROR:
            return {...state, errors: null};
        case RESET_MESSAGE:
            return {...state, message: null};
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails}
        case FETCH_USER_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails}
        case FETCH_USER_COCKTAILS_FAILURE:
            return {...state, errors: action.errors}
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktail: action.cocktail}
        case ADD_COCKTAIL_SUCCESS:
            return {...state, errors: null, message: action.message}
        case ADD_COCKTAIL_FAILURE:
            return {...state, errors: action.errors}
        case CHANGE_STATUS_SUCCESS:
            return {...state, errors: null, message: action.message}
        case CHANGE_STATUS_FAILURE:
            return {...state, errors: action.errors}
        case REMOVE_COCKTAIL_SUCCESS:
            return {...state, errors: null, message: action.message}
        case REMOVE_COCKTAIL_FAILURE:
            return {...state, errors: action.errors}
        case SET_RATING_SUCCESS:
            return {...state, errors: null, message: action.message}
        case SET_RATING_FAILURE:
            return {...state, errors: action.errors}
        default:
            return state;
    }
};

export default cocktailsReducer;