import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
      maxWidth: "150px",
      minWidth: "150px",
      minHeight: "300px",
      maxHeight: "350px",
      margin: "10px 0",
      padding: "20px",
      border: "1px solid",
      borderColor: "rgb(187, 195, 204)",
      textAlign: "center"
  },
  content: {
      width: "75%"
  },
  publish: {
    color: "red"
  }
});

const CardCocktail = (props) => {
  const classes = useStyles();

  return (
    <>
      {(props.publish || props.role === "admin") ? (
        <Card className={classes.root} onClick={props.blockHandler}>
          <CardActionArea>
            <CardMedia
              component="img"
              alt={props.name}
              height="150"
              image={props.image}
            />
            <CardContent className={classes.content}>
              <Typography gutterBottom variant="h5" component="h2">
                {props.name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.publish}>
                {(!props.publish) && ("Not published")}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      ) : null}
    </>
  );
}

export default CardCocktail;