import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 200,
        textDecoration: "none",
    },
    ingredient: {
        '&:hover': {
            textDecoration: "underline",
        }
    }
}));

const ListBlock = (props) => {
  const classes = useStyles();

  let ingredients = [];
if (props.ingredients) {
    ingredients = props.ingredients;
}

return (
    <List className={classes.root}>
        {ingredients.map(ingredient => (
            <ListItem key={ingredient._id}>
                <ListItemText key={ingredient._id}
                primary={`- ${ingredient.name} - ${ingredient.amount}` }
                className={classes.ingredient}
                />
            </ListItem>
        ))}
    </List>
);
}

export default ListBlock;