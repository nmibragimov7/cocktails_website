import React, { useEffect } from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const CocktailRating = (props) => {
    const [value, setValue] = React.useState(0);
    const ratingHandler = (newValue) => {
        setValue(newValue);
        props.setRatingHandler(newValue);
    };
    let ratings = [];
    if(props.ratings && props.ratings.length > 0) {
        ratings = props.ratings;
    }

    useEffect(()=> {
        ratings.forEach(rating => {
            if(rating.user === props.userId) {
                setValue(rating.rating);
            }
        })
    }, []);

    return (
        <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Rate</Typography>
        <Rating
            name="simple-controlled"
            value={value}
            onChange={(event, newValue) => {
                ratingHandler(newValue);
            }}
        />
        </Box>
    );
}

export default CocktailRating;