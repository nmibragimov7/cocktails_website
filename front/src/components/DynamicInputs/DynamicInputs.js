import React from 'react';
import Button from "@material-ui/core/Button";
import { Grid, TextField } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    inputsBlock: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        flexWrap: "nowrap"
    },
    btn: {
        padding: "6px 30px"
    }
}));

const DynamicInputs = (props) => {
    const classes = useStyles();

    const inputChangeHandler = index => event => {
        const {name, value} = event.target;
        let ingredientsCopy = [...props.state.ingredients];
        ingredientsCopy[index][name] = value;
        props.updateData(ingredientsCopy);
    };
  
    const ingredientRemoveHandler = index => event => {
        event.preventDefault()
        let ingredientsCopy = [...props.state.ingredients];
        ingredientsCopy.splice(index, 1);
        props.updateData(ingredientsCopy);
    }
  
    const addIngredient = event => {
        event.preventDefault();
        let ingredientsCopy = [...props.state.ingredients];
        ingredientsCopy.push({
            name: "",
            amount: ""
        });
        props.updateData(ingredientsCopy);
    };

    return (
        <>
            <Grid 
            item 
            container 
            className={classes.root} 
            spacing={2} 
            xs={12}>
                {props.state.ingredients.map((ingredient, i) => {
                    return (
                        <Grid 
                        key={i}
                        item 
                        container 
                        className={classes.inputsBlock} 
                        spacing={2} 
                        xs={8}
                        >
                            <Grid item xs={6}>
                                <TextField
                                    name="name"
                                    variant="outlined"
                                    required={true}
                                    id="name"
                                    value={ingredient.name}
                                    label="name"
                                    autoFocus
                                    onChange={inputChangeHandler(i)}
                                    error={!!props.getDynamicFieldError('name', i)}
                                    helperText={props.getDynamicFieldError('name', i)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    name="amount"
                                    variant="outlined"
                                    required={true}
                                    id="amount"
                                    value={ingredient.amount}
                                    label="amount"
                                    autoFocus
                                    onChange={inputChangeHandler(i)}
                                    error={!!props.getDynamicFieldError('amount', i)}
                                    helperText={props.getDynamicFieldError('amount', i)}
                                    fullWidth
                                />
                            </Grid>
                            {(i > 0) ? <Button variant="contained" className={classes.btn} onClick={ingredientRemoveHandler(i)}>Remove</Button> : null}
                        </Grid>
                    )
                })}
                <Button variant="contained" onClick={addIngredient}>Add ingredient</Button>

            </Grid>
            
        </>
    )
}

export default DynamicInputs;