import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { apiURL } from "../../config";
import CardCocktail from '../UI/CardCocktail/CardCocktail';
import ListBlock from '../UI/ListBlock/ListBlock';
import CocktailRating from '../UI/CocktailRating/CocktailRating';

const useStyles = makeStyles((theme) => ({
    root: {
        border: "1px solid",
        borderColor: "rgb(187, 195, 204)",
        padding: "20px",
    },
    content: {
        display: "flex",
        alignItems: "center",
    },
    image: {
        display: "block",
        minWidth: "150px",
        maxWidth: "250px",
        height: "auto",
        marginRight: "30px",    
    }
}));

const Cocktail = (props) => {
    const classes = useStyles();
    const [imageClasses, setImageClasses] = useState({});

    let image = "";
    if(props.image) {
        image = apiURL + "/uploads/" + props.image;
    }

    const errorHandler = () => {
        setImageClasses({visibility: "hidden"});
    };
    
    return (
        <>
            {props.full ? 
            (
                <>
                    {(props.publish || props.role) ? (
                        <div className={classes.root}>
                            <div className={classes.content}>
                                <img src={image} alt={props.name} className={classes.image} style={imageClasses} onError={errorHandler}/>
                                <div>
                                    <h1>{props.name}</h1>
                                    <h3>Rating: {props.averageRating} ({props.votes} votes)</h3>
                                    <p>Ingredients:</p>
                                    <ListBlock 
                                    ingredients={props.ingredients}
                                    />
                                </div>
                            </div>
                            <p>Recipe:</p>
                            <p>{props.recipe}</p>
                            {props.userId && 
                            <CocktailRating
                            userId={props.userId}
                            ratings={props.ratings}
                            setRatingHandler={props.setRatingHandler}
                            />
                            }
                        </div>
                    ) : null}
                </>
            ) : (
                <>
                    <CardCocktail
                    name={props.name}
                    image={image}
                    role={props.role}
                    publish={props.publish}
                    blockHandler={props.blockHandler}
                    />
                </>
            )}
        </>
        
    );
};

export default Cocktail;