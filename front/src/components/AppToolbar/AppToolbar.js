import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {Link, NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import {logoutUser} from "../../store/actions/usersAction";
import {useDispatch} from "react-redux";
import Avatar from '@material-ui/core/Avatar';
import {apiURL} from "../../config";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            opacity: "0.5"
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1
    },
    menu: {
        marginTop: "30px"
    }
}));
const AppToolbar = ({user}) => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logout = () => {
        dispatch(logoutUser());
    };

    let image = "";
    if(user && user.image) {
        image = apiURL + "/uploads/" + user.image;
    }

    return (
        <>
          <AppBar position="fixed">
            <Toolbar className={classes.toolbar}>
                <Typography variant="h6" className={classes.toolbarTitle}>
                    <Link to="/" className={classes.mainLink}>Cocktail builder</Link>
                </Typography>
                {user ? (
                    <>
                    <Avatar alt="Avatar" src={image} />
                    <Button
                        color="inherit"
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                    >
                        Hello, {user.displayName}
                    </Button>
                    <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    className={classes.menu}
                    >
                        {user && user.role === 'admin' ? (
                            <MenuItem onClick={handleClose}>
                                <Link to="/admin" className={classes.mainLink}>
                                    Admin
                                </Link>
                            </MenuItem>
                        ) : null}
                        <MenuItem onClick={handleClose}>
                            <Link to="/my_cocktails" className={classes.mainLink}>
                                My cocktails
                            </Link>
                        </MenuItem>
                        <MenuItem onClick={handleClose}>
                            <Link to="/new_cocktail" className={classes.mainLink}>
                                Create coctail
                            </Link>
                        </MenuItem>
                        <MenuItem onClick={logout}>Logout</MenuItem>
                    </Menu>
                    </>
                    ) : (
                        <>
                            <Button to="/login" className={classes.link} color="inherit" variant="outlined" component={NavLink}>Sign In</Button>
                            <Button to="/register" className={classes.link} color="inherit" variant="outlined" component={NavLink}>Sign Up</Button>
                        </>
                )}
            </Toolbar>
          </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;