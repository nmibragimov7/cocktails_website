import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import FileInput from "../UI/Form/FileInput";
import {useSelector} from "react-redux";
import TextInput from "../UI/Form/TextInput";
import DynamicInputs from '../DynamicInputs/DynamicInputs';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    }
}));

const AddCocktailForm = ({onSubmit}) => {
    const classes = useStyles();
    const [state, setState] = useState({
        name: "",
        ingredients: [{
            name: "",
            amount: ""
        }],
        recipe: "",
        image: "",
    });

    const errors = useSelector(state => state.cocktails.errors);

    const updateData = (data) => {
        setState(prevState => {
            return {...prevState, ingredients: data}
        })
    };

    const inputChangeHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        })
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState =>({
            ...prevState,
            [name]: file
            })
        )
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            if(key === "ingredients") {
                const ingredients = state[key].map(item => {
                    return JSON.stringify(item);
                });
                formData.append(key, JSON.stringify(ingredients));
            } 
            else {
                formData.append(key, state[key]);
            }
        });
        onSubmit(formData);
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const getDynamicFieldError = (fieldName, index) => {
        try {
            let error = "";
            Object.keys(errors.errors).forEach(key => {
                if(key.includes(fieldName) && key.includes(index)) {
                    error = errors.errors[key].message;
                }
            });
            return error;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
        <Grid container direction="column" spacing={2}>
            <TextInput
                label="name"
                onChange={inputChangeHandler}
                name="name"
                error={getFieldError('name')}
                required={true}
            />
            <DynamicInputs
                state={state}
                updateData={updateData}
                getDynamicFieldError={getDynamicFieldError}
            />
            <TextInput
                label="recipe"
                onChange={inputChangeHandler}
                name="recipe"
                error={getFieldError('recipe')}
                required={true}
            />
            <Grid item>
                <FileInput
                    name="image"
                    label="image"
                    onChange={fileChangeHandler}
                    error={getFieldError('image')}
                />
            </Grid> 
            <Grid item>
                <Button type="submit" color="primary" variant="contained">Create cocktail</Button>
            </Grid>
        </Grid>
        </form>
    );
};

export default AddCocktailForm;