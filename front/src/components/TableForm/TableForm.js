import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';
import { apiURL } from "../../config";

const useStyles = makeStyles({
    table: {
        width: "100%",
    },
    button: {
        width: "100px",
        marginLeft: "10px"
    },
    image: {
        display: "block",
        minWidth: "100px",
        maxWidth: "100px",
        height: "auto",
        margin: "0 auto",    
    }
});

const TableForm = (props) => {
    const classes = useStyles();
    const [imageClasses, setImageClasses] = useState({});

    let createData = (dataItem, action) => {
        return {dataItem, action};
    };

    if(props.tableName === "My cocktails") {
        createData = (image, name, publish) => {
            return {image, name, publish};
        };
    }

    const errorHandler = () => {
        setImageClasses({visibility: "hidden"});
    };

    let rows = [];
    props.data.forEach(item => {
        let image = "";
        if(item.image) {
            image = apiURL + "/uploads/" + item.image;
        }
        if(props.tableName === "My cocktails") {
            rows.push(
                createData(
                    (
                        <img key={item._id} src={image} alt={item.name} className={classes.image} style={imageClasses} onError={errorHandler}/>
                    ), 
                    item.name,
                    (
                        <>
                            {(!item.publish) ? (<p>Your cocktail is being moderated</p>) : null}
                        </>
                    )
                    )
            )
        }
        else {
            const dataItem = item;
            rows.push(
                createData(
                    dataItem, 
                    (
                        <>
                            <Button key={item._id} className={classes.button} variant="contained" color="secondary" onClick={() => {props.removeHandler(item._id)}}>Delete</Button>
                            {(!item.publish) && (
                                <Button className={classes.button} variant="contained" color="primary" onClick={() => {props.changeStatusHandler(item._id)}}>Publish</Button>
                            )}
                        </>
                    )
                    ),
            )
        }
    });

    let table = null;

    if(props.data.length > 0) {
        table = (
            <>
                <h1>{props.tableName}</h1>
                <TableContainer component={Paper}>
                    <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow key="head">
                                {(props.tableName === "My cocktails") ? (
                                    <>
                                        <TableCell align="center" key="image"><b>Image</b></TableCell>
                                        <TableCell align="center" key="name"><b>Name</b></TableCell>
                                        <TableCell align="center" key="published"><b>Published</b></TableCell>
                                    </>
                                ) : (
                                    <>
                                        <TableCell align="center" key="name"><b>Name</b></TableCell>
                                        <TableCell align="center" key="actions"><b>Actions</b></TableCell>
                                    </>
                                )}
                                
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row, i) => (
                                <>
                                    {(props.tableName === "My cocktails") ? (
                                        <TableRow key={i}>
                                            <TableCell align="center" key={i + 1}>
                                                {row.image}
                                            </TableCell>
                                            <TableCell align="center" key={i + 2}>
                                                {row.name}
                                            </TableCell>
                                            <TableCell align="center" key={i + 3}>
                                                {row.publish}
                                            </TableCell>
                                        </TableRow>
                                    ) : (
                                        <TableRow key={i}>
                                            <TableCell align="center" key={i + 1}>
                                                {row.dataItem.name}
                                            </TableCell>
                                            <TableCell align="center" key={i + 2}>
                                                {row.action}
                                            </TableCell>
                                        </TableRow>
                                    )}
                                </>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </>
        )
    }

    return (
        <>
            {table}
        </>
    );
}

export default TableForm;