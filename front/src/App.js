import React from 'react';
import Container from "@material-ui/core/Container";
import { Route, Switch, Redirect } from "react-router-dom"
import AppToolbar from "./components/AppToolbar/AppToolbar";
import Register from "./containers/User/Register";
import Login from "./containers/User/Login";
import Cocktails from "./containers/Cocktails/Cocktails";
import Admin from './containers/Admin/Admin';
import FullCocktail from './containers/FullCocktail/FullCocktail';
import AddCocktail from './containers/AddCocktail/AddCocktail';
import MyCocktails from './containers/MyCocktails/MyCocktails';
import { useSelector } from "react-redux";

const App = (props) => {
  const user = useSelector(store => store.users.user);

  return(
    <>
      <header><AppToolbar user={user}/></header>
      <main>
        <Container>
            <Switch>
              <Route path="/" exact component={Cocktails}/>
              <ProtectRoute path="/admin" isAllowed={user && user.role === 'admin'} redirectTo={'/'} exact component={Admin} />
              <Route path="/register" exact component={Register}/>
              <Route path="/login" exact component={Login}/>
              <ProtectRoute path="/new_cocktail" isAllowed={user} redirectTo={'/'} exact component={AddCocktail} />
              <ProtectRoute path="/my_cocktails" isAllowed={user} redirectTo={'/'} exact component={MyCocktails} />
              <Route path="/:id" component={FullCocktail}/>
              <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
            </Switch>
        </Container>
      </main>
    </>
  );
};

const ProtectRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo} />
};

export default App;
