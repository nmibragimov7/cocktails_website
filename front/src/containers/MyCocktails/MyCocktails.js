import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchUserCocktails, resetError } from "../../store/actions/cocktailsActions";
import TableForm from '../../components/TableForm/TableForm';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const MyCocktails = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const cocktails = useSelector(state => state.cocktails.cocktails);
    const errors = useSelector(state => state.cocktails.errors);

    useEffect(()=> {
        dispatch(resetError());
        dispatch(fetchUserCocktails());
    }, [dispatch]);

    return (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            <TableForm
            tableName="My cocktails"
            data={cocktails}
            />
        </>
    );
};

export default MyCocktails;