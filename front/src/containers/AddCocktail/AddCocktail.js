import React, {useEffect} from 'react';
import { fetchCocktails, addCocktail, resetError } from '../../store/actions/cocktailsActions';
import { useDispatch, useSelector } from "react-redux";
import AddCocktailForm from '../../components/AddCocktailForm/AddCocktailForm';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    }
}));

const AddCocktail = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const errors = useSelector(state => state.cocktails.errors);

    useEffect(()=> {
        dispatch(resetError());
        dispatch(fetchCocktails());
    }, [dispatch]);

    const onSubmit = (data) => {
        dispatch(addCocktail(data));
    };

    return (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            <AddCocktailForm 
            onSubmit={onSubmit}
            />
        </>
    );
};

export default AddCocktail;