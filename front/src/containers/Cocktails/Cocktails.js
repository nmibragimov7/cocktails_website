import React, {useEffect} from 'react';
import Grid from "@material-ui/core/Grid";
import { useDispatch, useSelector } from "react-redux";
import { fetchCocktails, resetMessage} from "../../store/actions/cocktailsActions";
import { loadingHandler } from "../../store/actions/loadingActions";
import Cocktail from "../../components/Cocktail/Cocktail";
import Spinner from "../../components/Spinner/Spinner";
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const Cocktails = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const message = useSelector(state => state.cocktails.message);
    const loading = useSelector(state => state.loading.loading);

    useEffect(()=> {
        dispatch(loadingHandler());
        dispatch(fetchCocktails());
    }, [dispatch]);

    const blockHandler = (id) => {
        props.history.push({
            pathname: `/${id}`,
        });
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    }

    let role = "";
    if (user && user.role === "admin") {
        role = "admin";
    }

    let form = (
        <>
            <h1>Coctails:</h1>
            <Grid container direction="row" spacing={2}>
                {(user && user.role === "admin") ? (
                    <>
                        {cocktails.map(cocktail => (
                            <Grid item key={cocktail._id}>
                                <Cocktail
                                id={cocktail._id}
                                name={cocktail.name}
                                image={cocktail.image}
                                blockHandler={() => blockHandler(cocktail._id)}
                                full={false}
                                role={role}
                                publish={cocktail.publish}
                                />  
                            </Grid>
                        ))}
                    </>
                ) : (
                    <>
                        {cocktails.map(cocktail => 
                            {
                                if(cocktail.publish) {
                                    return (
                                        <Grid item key={cocktail._id}>
                                            <Cocktail
                                            id={cocktail._id}
                                            name={cocktail.name}
                                            image={cocktail.image}
                                            blockHandler={() => blockHandler(cocktail._id)}
                                            full={false}
                                            role={role}
                                            publish={cocktail.publish}
                                            />  
                                        </Grid>
                                    )
                                }
                                return null;
                            }
                        )}
                    </>
                )}
            </Grid>
        </>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default Cocktails;