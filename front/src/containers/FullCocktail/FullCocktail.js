import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchCocktail, setRating, resetError, resetMessage } from "../../store/actions/cocktailsActions";
import Cocktail from '../../components/Cocktail/Cocktail';
import { loadingHandler } from "../../store/actions/loadingActions";
import Spinner from "../../components/Spinner/Spinner";
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const FullCocktail = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    
    const user = useSelector(state => state.users.user);
    const cocktail = useSelector(state => state.cocktails.cocktail);
    const errors = useSelector(state => state.cocktails.errors);
    const message = useSelector(state => state.cocktails.message);
    const loading = useSelector(state => state.loading.loading);

    useEffect(()=> {
        dispatch(resetError());
        dispatch(resetMessage());
        dispatch(loadingHandler());
        dispatch(fetchCocktail(props.match.params.id));
    }, [dispatch]);

    let role = "";
    if (user && user.role === "admin") {
        role = "admin";
    }
    let userId = "";
    if (user) {
        userId = user._id;
    }

    const setRatingHandler = (rating) => {
        dispatch(setRating(cocktail._id, rating));
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    let form = (
        <>
            <Cocktail
            id={cocktail._id}
            name={cocktail.name}
            image={cocktail.image}
            recipe={cocktail.recipe}
            full={true}
            role={role}
            userId={userId}
            ingredients={cocktail.ingredients}
            publish={cocktail.publish}
            averageRating={cocktail.averageRating}
            votes={cocktail.votes}
            ratings={cocktail.ratings}
            setRatingHandler={setRatingHandler}
            />
        </>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default FullCocktail;