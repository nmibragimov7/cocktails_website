import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchCocktails, removeCocktail, changeStatus, resetError, resetMessage } from "../../store/actions/cocktailsActions";
import TableForm from '../../components/TableForm/TableForm';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const Admin = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const cocktails = useSelector(state => state.cocktails.cocktails);
    const errors = useSelector(state => state.cocktails.errors);
    const message = useSelector(state => state.cocktails.message);

    useEffect(()=> {
        dispatch(resetError());
        dispatch(resetMessage());
        dispatch(fetchCocktails());
    }, [dispatch]);

    const removeHandler = (id) => {
        dispatch(removeCocktail(id));
    };  

    const changeStatusHandler = (id) => {
        dispatch(changeStatus(id));
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    return (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            <TableForm
            tableName="Cocktails"
            data={cocktails}
            removeHandler={removeHandler}
            changeStatusHandler={changeStatusHandler}
            />
        </>
    );
};

export default Admin;